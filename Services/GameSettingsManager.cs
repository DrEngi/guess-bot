﻿using guess_bot.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace guess_bot.Services
{
    public class GameSettingsManager
    {
        private static Dictionary<ulong, ServerGuessGame> games;

        public GameSettingsManager()
        {
            games = new Dictionary<ulong, ServerGuessGame>();
        }

        /// <summary>
        /// Gets a game for a specified server or creates a new one if it does not exist.
        /// </summary>
        /// <param name="serverID">the ID of the server oyu want to find</param>
        /// <returns></returns>
        public static ServerGuessGame getGame(ulong serverID)
        {
            if (games.ContainsKey(serverID)) return games[serverID];
            else
            {
                ServerGuessGame game = new ServerGuessGame(serverID);
                games.Add(serverID, game);
                return game;
            }
        }
    }
}
