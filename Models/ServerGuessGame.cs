﻿using System;
using System.Collections.Generic;
using System.Text;

namespace guess_bot.Models
{
    public class ServerGuessGame
    {
        public int Number = -1;
        public int Tries = 0;
        public bool GameRunning = false;
        public ulong ChannelID = 0;

        public int LowerBound = 1;
        public int UpperBound = 1000;


        public string LastWinnerMention = "";
        public int LastWinnerCount = 0;
        public int LastWinnerTries = 0;
        public bool HadLastWinner = false;

        public ulong ServerID = 0;

        public ServerGuessGame(ulong serverID)
        {
            ServerID = serverID;
        }
    }
}
