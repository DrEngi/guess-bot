using Discord.Commands;
using Discord;
using Discord.WebSocket;
using System.Threading.Tasks;
using Discord.Rest;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Diagnostics;
using System.Threading;
using guess_bot.Models;
using System;

namespace guess_bot
{
    public class GuessModule : ModuleBase
    {
        [Command("lastwinner"), Summary("Gets the last winner")]
        [RequireUserPermission(ChannelPermission.ManageMessages)]
        public async Task LastWinner()
        {
            ServerGuessGame currentGame = Services.GameSettingsManager.getGame(Context.Guild.Id);
            if (currentGame.HadLastWinner)
            {
                var emb = new EmbedBuilder();
                emb.WithTitle("Last Winner");
                emb.WithCurrentTimestamp();
                emb.WithColor(Color.Green);

                emb.AddField("Winner", currentGame.LastWinnerMention);
                emb.AddField("Tries", currentGame.LastWinnerTries);
                emb.AddField("Number", currentGame.LastWinnerCount);
                
                await ReplyAsync("", false, emb.Build());
            }
            else
            {
                await ReplyAsync("There hasn't been a winner yet.");
            }
        }

        [Command("setnumber"), Summary("Set a number for the guess command")]
        [RequireUserPermission(ChannelPermission.ManageMessages)]
        public async Task SetNumber([Summary("The lower bound")] int lowerBound = 1, [Summary("The lower bound")] int upperBound = 1000, [Summary("The number to set the guess to")] int number = -1)
        {
            ServerGuessGame currentGame = Services.GameSettingsManager.getGame(Context.Guild.Id);
            if (number == -1)
            {
                currentGame.LowerBound = lowerBound;
                currentGame.UpperBound = upperBound;

                Random rnd = new Random();
                currentGame.Number = rnd.Next(currentGame.LowerBound, currentGame.UpperBound);
                await ReplyAsync("Guess number set to a random number between " + currentGame.LowerBound + " and " + currentGame.UpperBound + ". Good luck.");
            }
            else
            {
                currentGame.Number = number;
                await ReplyAsync("Guess number set to " + currentGame.Number);
            }
        }

        [Command("setchannel"), Summary("Set the specified game channel")]
        [RequireUserPermission(ChannelPermission.ManageMessages)]
        public async Task SetChannel(IChannel channel)
        {
            ServerGuessGame currentGame = Services.GameSettingsManager.getGame(Context.Guild.Id);
            currentGame.ChannelID = channel.Id;
            await ReplyAsync("Channel set.");
        }

        public static async Task GuessNumber(int number, ISocketMessageChannel channel, IUser user, IGuildChannel guildChannel, CommandContext context)
        {
            ServerGuessGame currentGame = Services.GameSettingsManager.getGame(context.Guild.Id);
            if (currentGame.GameRunning)
            {
                if (number == currentGame.Number)
                {
                    currentGame.GameRunning = false;

                    currentGame.LastWinnerMention = user.Mention;
                    currentGame.LastWinnerCount = currentGame.Number;
                    currentGame.LastWinnerTries = currentGame.Tries + 1;
                    currentGame.HadLastWinner = true;

                    currentGame.Tries++;
                    currentGame.Number = -1;
                    int oldtries = currentGame.Tries;
                    currentGame.Tries = 0;

                    var role = context.Guild.GetRole(context.Guild.Roles.FirstOrDefault(x => x.Name.Equals("Member")).Id);
                    var channelNew = await context.Guild.GetChannelAsync(currentGame.ChannelID);

                    // If either the of the object does not exist, bail
                    if (role == null || channelNew == null) return;

                    // Fetches the previous overwrite and bail if one is found
                    var previousOverwrite = channelNew.GetPermissionOverwrite(role);
                    if (previousOverwrite.HasValue) await channelNew.RemovePermissionOverwriteAsync(role);

                    // Creates a new OverwritePermissions with send message set to deny and pass it into the method
                    await channelNew.AddPermissionOverwriteAsync(role, new OverwritePermissions(sendMessages: PermValue.Deny));

                    try
                    {
                        RestUserMessage msg = await channel.SendMessageAsync("Congratulations " + user.Mention + ", " + number + " is correct! It took " + oldtries + " tries to guess it.");
                        Thread.Sleep(20);
                        await msg.PinAsync();
                    }
                    catch (Discord.Net.HttpException ex)
                    {
                        Debug.Print(ex.ToString());
                    }

                }
                else
                {
                    if (number > currentGame.UpperBound || number < currentGame.LowerBound)
                    {
                        await channel.SendMessageAsync("Hey, " + user.Mention + ", your number is not between " + currentGame.LowerBound + " and " + currentGame.UpperBound + ". Like the channel topic says. ~~Dumbass.~~");
                    }

                    currentGame.Tries++;
                }
            }
        }

        [Command("start"), Summary("Start the game")]
        [RequireUserPermission(ChannelPermission.ManageMessages)]
        public async Task StartGame()
        {
            ServerGuessGame currentGame = Services.GameSettingsManager.getGame(Context.Guild.Id);
            if (!currentGame.GameRunning)
            {
                if (currentGame.ChannelID != 0)
                {
                    if (currentGame.Number != -1)
                    {
                        currentGame.GameRunning = true;

                        var role = Context.Guild.GetRole(Context.Guild.Roles.FirstOrDefault(x => x.Name.Equals("Member")).Id);
                        var channelNew = await Context.Guild.GetChannelAsync(currentGame.ChannelID);

                        // If either the of the object does not exist, bail
                        if (role == null || channelNew == null) return;

                        // Fetches the previous overwrite and bail if one is found
                        var previousOverwrite = channelNew.GetPermissionOverwrite(role);
                        if (previousOverwrite.HasValue) await channelNew.RemovePermissionOverwriteAsync(role);

                        // Creates a new OverwritePermissions with send message set to deny and pass it into the method
                        await channelNew.AddPermissionOverwriteAsync(role,
                            new OverwritePermissions(sendMessages: PermValue.Allow));
                        
                        await ReplyAsync("A game has started!");
                        await (await Context.Guild.GetTextChannelAsync(currentGame.ChannelID)).SendMessageAsync("A game has started. Guess the number between " + currentGame.LowerBound + " and " + currentGame.UpperBound + " No decimals.");

                        await (await Context.Guild.GetTextChannelAsync(currentGame.ChannelID)).ModifyAsync(x =>
                        {
                            x.Topic = "Guess the number between " + currentGame.LowerBound + " and " + currentGame.UpperBound + ". No decimals. Good luck!";
                        });
                    }
                    else
                    {
                        await ReplyAsync("A number has not been set yet. Set it with `g. setnumber <number>`");
                    }
                }
                else
                {
                    await ReplyAsync("A channel has not been set. Set the channel with `g. setchannel` in the correct channel.");
                }
            }
            else
            {
                await ReplyAsync("A game is already running");
            }
        }

        [Command("stop"), Summary("Stops the game")]
        [RequireUserPermission(ChannelPermission.ManageMessages)]
        public async Task StopGame()
        {
            ServerGuessGame currentGame = Services.GameSettingsManager.getGame(Context.Guild.Id);
            if (currentGame.GameRunning)
            {
                currentGame.GameRunning = false;

                var role = Context.Guild.GetRole(Context.Guild.Roles.FirstOrDefault(x => x.Name.Equals("Member")).Id);
                var channelNew = await Context.Guild.GetChannelAsync(currentGame.ChannelID);

                // If either the of the object does not exist, bail
                if (role == null || channelNew == null) return;

                // Fetches the previous overwrite and bail if one is found
                var previousOverwrite = channelNew.GetPermissionOverwrite(role);
                if (previousOverwrite.HasValue) await channelNew.RemovePermissionOverwriteAsync(role);

                // Creates a new OverwritePermissions with send message set to deny and pass it into the method
                await channelNew.AddPermissionOverwriteAsync(role,
                    new OverwritePermissions(sendMessages: PermValue.Deny));

                await ReplyAsync("A game has been stoppoed");
            }
            else
            {
                await ReplyAsync("A game is not running");
            }
        }
    }
}